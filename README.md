# ⚡ vim-488 ⚡

A Vim plugin for the CSC488 language.

## Innstallation

Use your preferred plugin manager. Personally I like [vim-plug](https://github.com/junegunn/vim-plug)

```vimL
Plug 'isthisnagee/vim-488', { 'for': '488' }
```

## Examples

![flatwhite](images/flatwhite.png)
![solarized-light](images/solarized-light.png)
![default](images/default.png)
![pablo](images/pablo.png)



